#include <stdio.h>
#include <stdlib.h>
#include <sys/vfs.h>

static const char *partitions[] = { "/", "/data", NULL };
static const char *icon = "󰭯";

int
main(int argc, char **argv)
{
  struct statfs stats;
  float avail;
  char buf[BUFSIZ];
  char *bufp;
  int i;

  printf("%s ", icon);
  for (i = 0, bufp = buf; partitions[i] != NULL; i++)
  {
    if (statfs(partitions[i], &stats) == -1)
    {
      bufp += sprintf(bufp, "%s: Unknown - ", partitions[i]);
      continue;
    }
    avail = (float) (stats.f_bsize * stats.f_bavail) / 1073741824;
    bufp += sprintf(bufp, "%s: %.1fG - ", partitions[i], avail);
  }
  *(bufp - 3) = '\0';

  printf("%s", buf);

  return EXIT_SUCCESS;
}
