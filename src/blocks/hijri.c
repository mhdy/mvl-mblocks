#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <itl/hijri.h>

int
main(int argc, char **argv)
{
  sDate hijri;
  int day, month, year;
  time_t now;
  struct tm* tm_info;

  now = time(NULL);
  tm_info = localtime(&now);

  day = tm_info->tm_mday;
  month = tm_info->tm_mon + 1;
  year = tm_info->tm_year + 1900;

  if(!h_date(&hijri, day, month, year))
    printf("%d %s %d", hijri.day, hijri.to_mname, hijri.year);
  else
    puts("Unknow");

  free(hijri.event);

  return EXIT_SUCCESS;
}
