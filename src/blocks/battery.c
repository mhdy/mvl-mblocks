#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

static const int mode = 1;

static const char *batteries[] = {
  "/sys/class/power_supply/BAT0",
  "/sys/class/power_supply/BAT1",
  NULL
};

static const char *ac = "/sys/class/power_supply/AC/online";

static const char *icons[2][11] = {
  { "󰂎", "󰁺", "󰁻", "󰁼", "󰁽", "󰁾", "󰁿", "󰂀", "󰂁", "󰂂", "󰁹" },
  { "󰢟", "󰢜", "󰂆", "󰂇", "󰂈", "󰢝", "󰂉", "󰢞", "󰂊", "󰂋", "󰂅" }
};
static const char *charging_icon = "󱐋";
static const char *err_icon = "󱉞";

char *
get_info(char *buf, const char *which, ...)
{
  char path[BUFSIZ];
  va_list args;
  char *arg;
  FILE *fp;

  va_start(args, which);
  strcpy(path, which);
  while ((arg = va_arg(args, char *)) != NULL)
  {
    strcat(path, "/");
    strcat(path, arg);
  }
  va_end(args);
  fp = fopen(path, "r");
  if (!fp)
    return NULL;
  fscanf(fp, "%s", buf);
  fclose(fp);
  return buf;
}

int
get_ac_status(void)
{
  char buf[2];
  if (!get_info(buf, ac, NULL))
    return -1;
  return atoi(buf);
}

int
get_battery_status(const char *battery)
{
  char buf[BUFSIZ];
  if (!get_info(buf, battery, "status", NULL))
    return -1;
  return strcmp(buf, "Charging") == 0;
}

float
get_capacity(const char *battery)
{
  char buf[BUFSIZ];
  float capacity;
  if (!get_info(buf, battery, "capacity", NULL))
    return -1;
  if ((capacity = atof(buf)) > 100)
    capacity = 100;
  return capacity;
}

float
get_energy_full(const char *battery)
{
  char buf[BUFSIZ];
  if (!get_info(buf, battery, "energy_full", NULL))
    return -1;
  return atof(buf);
}

char *
get_icon(int status, float capacity)
{
  return (char *)icons[status][(int)(capacity / 10)];
}

void
display_individual_status(void)
{
  int ac_status;
  int battery_status;
  float capacity, energy_full;
  char buf[BUFSIZ];
  char *bufp;
  int i;

  ac_status = get_ac_status();
  if (ac_status == -1)
  {
    printf("%s Unknown", err_icon);
    exit(EXIT_FAILURE);
  }
  for (i = 0, bufp = buf; batteries[i] != NULL; i++)
  {
    battery_status = get_battery_status(batteries[i]);
    capacity = get_capacity(batteries[i]);
    energy_full = get_energy_full(batteries[i]);
    if (battery_status == -1 || capacity == -1 || energy_full == -1)
      continue;
    capacity = ceil(capacity);
    bufp += sprintf(bufp, "%s%.0f%% ", get_icon(battery_status, capacity), capacity);
  }
  *(bufp - 1) = '\0';
  printf("%s", buf);
}

void
display_combined_status(void)
{
  int ac_status;
  float capacity, energy_full;
  int i;
  float a, b;

  ac_status = get_ac_status();
  if (ac_status == -1)
  {
    printf("%s Unknown", err_icon);
    exit(EXIT_FAILURE);
  }
  a = b = 0;
  for (i = 0; batteries[i] != NULL; i++)
  {
    capacity = get_capacity(batteries[i]);
    energy_full = get_energy_full(batteries[i]);
    if (capacity == -1 || energy_full == -1)
      continue;
    a += capacity * energy_full;
    b += energy_full;
  }
  capacity = ceil(a / b);
  printf("%s %.0f%% ", get_icon(ac_status, capacity), capacity);
}

int
main(int argc, char **argv)
{
  switch (mode)
  {
    case 0:
      display_combined_status();
      break;
    case 1:
      display_individual_status();
      break;
    default:
      display_combined_status();
      break;
  }
  return EXIT_SUCCESS;
}
