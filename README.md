# mblocks

[mblocks](https://gitlab.com/mhdy/mblocks) is a multi-threaded status monitor written in Rust.
It updates the status only when there is a change.

This is the production version I use in my desktop environment.

To install, first clone this repository.

```
git clone https://gitlab.com/mhdy/mblocks-mvl.git
```

Then, install using `make clean install`.
