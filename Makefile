PREFIX ?= ~/.local
CC ?= cc
RUST_DOCKER_TAG = 1.71-bullseye

cblocks: src/blocks/*.c
	mkdir -p target/cblocks/
	${CC} -o target/cblocks/mblocks-battery src/blocks/battery.c -lm
	# ${CC} -o target/cblocks/mblocks-hijri-date src/blocks/hijri.c -litl
	${CC} -o target/cblocks/mblocks-disk src/blocks/disk.c

build: cblocks
	docker container run --rm -v "$(shell pwd):/workspace" \
		rust:$(RUST_DOCKER_TAG) sh -c 'cd /workspace && cargo build --release'

install: build
	mkdir -p $(PREFIX)/bin/
	install -m 0700 \
		target/release/mblocks \
		target/cblocks/mblocks-* \
		src/blocks/mblocks-* \
		$(PREFIX)/bin/
	mkdir -p $(PREFIX)/share/fonts/
	install -m 0700 fonts/mde-* $(PREFIX)/share/fonts/

clean:
	rm -rf target/

uninstall:
	rm -f $(PREFIX)/bin/mblocks $(PREFIX)/bin/mblocks-*

.PHONY: build cblocks install clean uninstall
